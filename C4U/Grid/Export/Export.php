<?php

namespace C4U\Grid\Export;

class Export extends \Grido\Components\Export {

	/** @var ExportStrategy */
	private $strategy;

	public function setStrategy(ExportStrategy $strategy) {
		$this->strategy = $strategy;
	}

	public function send(\Nette\Http\IRequest $httpRequest, \Nette\Http\IResponse $httpResponse) {
		if (!$this->strategy) throw new \Exception('No strategy selected');

		$filename = $this->label;
		$data = $this->grid->getData(FALSE);
		$columns = $this->grid[\Grido\Components\Columns\Column::ID]->getComponents();

		$this->strategy->getData($filename, $data, $columns);
	}


}