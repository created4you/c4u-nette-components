<?php

namespace C4U\Grid\Export;

interface ExportStrategy {

	public function getData($filename, $data, $columns);

}
