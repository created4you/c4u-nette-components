<?php

namespace C4U\Grid\Filters;

use Grido\Components\Filters\Filter;
use Grido\Exception;

class Bool3StateFilter extends Filter {

	public function __construct(\Grido\Grid $grid, $name, $label) {
		parent::__construct($grid, $name, $label);
		$this->getControl()->setItems(array(
			null => '-',
			0 => 'Ne',
			1 => 'Ano',
		));
	}

	/**
	 * @return \Nette\Forms\Controls\SelectBox
	 */
	protected function getFormControl() {
		return new \Nette\Forms\Controls\SelectBox($this->label);
	}


}
