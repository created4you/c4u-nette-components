<?php

namespace C4U\Grid\Columns;

use C4U\Components\Helpers\BoolHelper;
use Grido\Components\Columns\Editable;

class Bool3State extends Editable {

	protected function formatValue($value) {
		return BoolHelper::formatBool3State($value);
	}

}
