<?php

namespace C4U\Grid;

use C4U\Components\Helpers\Helpers;
use C4U\Grid\Columns\Bool3State;
use C4U\Grid\Export\ExportStrategy;
use C4U\Grid\Filters\Bool3StateFilter;
use Grido\Grid;
use Grido\Translations\FileTranslator;
use Grido\Components\Filters\Filter;
use Nette\ComponentModel\IContainer;
use C4U\Grid\Export\Export;
use Nette\Utils\Html;

abstract class BaseGrid extends Grid {

	protected $translator;
	protected $export;
	protected $exportType;
	private $formatCallbacks = array();

	public function __construct(
		$id,
		FileTranslator $fileTranslator
	) {
		parent::setParent(null, $id);
		$this->translator = $fileTranslator;
	}

	public function prepare() {
		$this->setModel($this->prepareModel());
		$this->setColumns();
		$this->setFilters();
		$this->setCommon();
		$this->setActions();
	}

	protected function setCommon() {
		$this->setFilterRenderType(Filter::RENDER_INNER);
		$this->setTranslator($this->translator);
		$this->perPage = 100;
		$this->perPageList = array(100);
//		$this->setRememberState();

		// Run formatting functions when there is some callback.
		if (count($this->formatCallbacks)) $this->onFetchData[] = $this->format($this);
	}

	abstract protected function prepareModel();

	abstract protected function setColumns();

	abstract protected function setFilters();

	abstract protected function setActions();

	protected function dateFormat($value) {
		$date = explode('.', $value);
		foreach ($date as &$val) {
			$val = (int)$val;
		}

		return count($date) == 3
			? array('[$value] = %s', "{$date[2]}-{$date[1]}-{$date[0]}")
			: NULL;
	}

	protected function prepareLink($operation, $params) {
		return $this->presenter->link($operation, $params);
	}

	protected function formatPrice($price, $precision = 2, $currency = 'CZK') {
		if (!$price) return "";
		return Helpers::currencyFloat($price, $precision, $currency);
	}

	protected function formatPriceNbsp($price, $precision = 2, $currency = 'CZK') {
		$el = Html::el('span');
		return $el->setHtml(Helpers::currencyFloatNbsp($price, $precision, $currency));
	}

	protected function formatBool($value, $yesValue = 'Ano', $noValue = '') {
		return $value ? $yesValue: $noValue;
	}

	protected function formatPercent($value, $decimals = null) {
		if (!$value || $value > 1 || $value < 0) return "";
		if ($decimals) {
			return round($value * 100, $decimals) . ' %';
		} else {
			return ($value * 100) . ' %';
		}
	}

	/**
	 * Export.
	 */

	public function setExportStrategy(ExportStrategy $strategy) {
		$this->export->setStrategy($strategy);
	}

	public function setExport($label = NULL) {
		$this->export = new Export($this, $label);
		return $this->export;
	}

	protected function addFormatCallback($name, $obj) {
		if (!array_key_exists($name, $this->formatCallbacks)) {
			$this->formatCallbacks[$name] = array();
		}

		if (!is_array($this->formatCallbacks[$name])) {
			$this->formatCallbacks[$name] = array();
		}

		$this->formatCallbacks[$name][] = $obj;
	}

	protected function format(BaseGrid $grid) {
		$output = array();
		foreach ($grid->getData() as $row) {
			$this->applyCallback($row);
			$output[] = $row;
		}
		return $output;
	}

	private function applyCallback(&$row) {
		foreach ($this->formatCallbacks as $key => $callbacks) {
			if (isset($row->{$key})) {
				foreach ($callbacks as $callback) {
					$row->{$key} = call_user_func_array(array($this, $callback), array($row->{$key}));
				}
			}
		}
	}

	/**
	 * Custom cols.
	 */
	public function addColumnBool3State($name, $label) {
		return new Bool3State($this, $name, $label);
	}

	public function addFilterBool3State($name, $label) {
		return new Bool3StateFilter($this, $name, $label);
	}

}
