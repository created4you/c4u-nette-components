<?php

namespace C4U\Grid\ExportStrategy;

use C4U\Components\XlsGenerator\XlsGenerator;
use C4U\Grid\Export\ExportStrategy;
use Grido\Components\Columns\Date;

class XlsExportStrategy extends XlsGenerator implements ExportStrategy {

	const HEADER_STARTS_AT = 1;
	const DATA_STARTS_AT = 2;

	private $filename;
	private $sheetName;

	public function getData($filename, $data, $columns) {
		$this->createHeader($columns);
		$this->createData($columns, $data);

		$this->filename = $filename;
		$this->publish();
	}

	public function setSheetName($sheetName) {
		$sheetName = str_replace(array('?'), '', $sheetName);
		$sheetName = substr($sheetName, 0, 30);
		$this->sheetName = $sheetName;
	}

	protected function getFilename() {
		return $this->filename;
	}

	protected function createStructure() {

	}

	private function createHeader($columns) {
		/** @var \PHPExcel_Worksheet $sheet */
		$sheet = $this->workbook->getActiveSheet();
		if ($this->sheetName) {
			$sheet->setTitle($this->sheetName);
		}

		$i = 0;
		$headerRow = self::HEADER_STARTS_AT;
		foreach ($columns as $column) {
			$sheet->getCell(\PHPExcel_Cell::stringFromColumnIndex($i) . $headerRow)->setValue($column->getLabel());

			$sheet->getColumnDimension(\PHPExcel_Cell::stringFromColumnIndex($i))->setAutoSize(true);
			$i++;
		}
	}

	private function createData($columns, $data) {
		/** @var \PHPExcel_Worksheet $sheet */
		$sheet = $this->workbook->getActiveSheet();

		$line = self::DATA_STARTS_AT;
		$colIndex = 0;
		$colName = \PHPExcel_Cell::stringFromColumnIndex($colIndex);
		foreach ($data as $item) {
			foreach ($columns as $column) {
				if ($column instanceof Date) {
					\C4U\Grid\ExportStrategy\ColumnType\Date::render($column,$item, $sheet, $colName, $line);
				} else {
					$sheet->getCell($colName . $line)->setValue(html_entity_decode($column->renderExport($item)));
				}
				$colName = \PHPExcel_Cell::stringFromColumnIndex(++$colIndex);
			}
			$line++;
			$colIndex = 0;
			$colName = \PHPExcel_Cell::stringFromColumnIndex($colIndex);
		}
	}

}