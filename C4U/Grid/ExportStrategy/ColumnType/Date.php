<?php

namespace C4U\Grid\ExportStrategy\ColumnType;

use C4U\Date\DateFactory;

/**
 * Class Date
 * Map grido column types to PHPExcel columns.
 * @package C4U\Grid\ExportStrategy\ColumnType
 */
class Date {

	const FORMAT_DATE = 'd.m.Y';
	const FORMAT_DATETIME = 'd.m.Y H:i:s';

	public static function render(\Grido\Components\Columns\Date $column, \DibiRow $row, \PHPExcel_Worksheet $sheet, $colName, $line) {
		if (!$column->renderExport($row)) return;

		if ($column->getDateFormat() === self::FORMAT_DATETIME) {
			$date = DateFactory::fromSqlDateTime($column->renderExport($row));
		} else {
			$date = DateFactory::fromCzechDate($column->renderExport($row));
		}
		$sheet->getCell($colName . $line)->setValue(\PHPExcel_Shared_Date::PHPToExcel($date->toUnixTime()));
		$sheet->getStyle($colName . $line)->getNumberFormat()->setFormatCode(self::getDateFormat($column->getDateFormat()));
	}

	private static function getDateFormat($columnFormat) {
		switch ($columnFormat) {
			case self::FORMAT_DATETIME: return "dd.mm.YYYY h:mm:ss";
			default: return 'dd.mm.YYYY';
		}
	}

}