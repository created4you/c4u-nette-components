<?php

namespace C4U\DataGrid;

use C4U\Components\Helpers\Helpers;
use Nette\Utils\Html;
use Ublaboo\DataGrid\DataGrid;

abstract class BaseGrid extends DataGrid {

	protected $translator;
	protected $export;
	protected $exportType;
	private $formatCallbacks = array();

	public function prepare() {
		$this->setDataSource($this->prepareDataSource());
		$this->setColumns();
		$this->setFilters();
		$this->setCommon();
		$this->setActions();
	}

	protected function setCommon() {
		$this->setDefaultPerPage(100);
		$this->setItemsPerPageList([100]);
	}

	abstract protected function prepareDataSource();

	abstract protected function setColumns();

	abstract protected function setFilters();

	abstract protected function setActions();

	protected function dateFormat($value) {
		$date = explode('.', $value);
		foreach ($date as &$val) {
			$val = (int)$val;
		}

		return count($date) == 3
			? array('[$value] = %s', "{$date[2]}-{$date[1]}-{$date[0]}")
			: NULL;
	}

	protected function prepareLink($operation, $params) {
		return $this->presenter->link($operation, $params);
	}

	protected function formatPrice($price, $precision = 2, $currency = 'CZK') {
		if (!$price) return "";
		return Helpers::currencyFloat($price, $precision, $currency);
	}

	protected function formatPriceNbsp($price, $precision = 2, $currency = 'CZK') {
		$el = Html::el('span');
		return $el->setHtml(Helpers::currencyFloatNbsp($price, $precision, $currency));
	}

	protected function formatBool($value, $yesValue = 'Ano', $noValue = '') {
		return $value ? $yesValue: $noValue;
	}

	protected function formatPercent($value, $decimals = null) {
		if (!$value || $value > 1 || $value < 0) return "";
		if ($decimals) {
			return round($value * 100, $decimals) . ' %';
		} else {
			return ($value * 100) . ' %';
		}
	}

	/**
	 * Export.
	 */

	public function setExportStrategy(ExportStrategy $strategy) {
		$this->export->setStrategy($strategy);
	}

	public function setExport($label = NULL) {
		$this->export = new Export($this, $label);
		return $this->export;
	}

	protected function addFormatCallback($name, $obj) {
		if (!array_key_exists($name, $this->formatCallbacks)) {
			$this->formatCallbacks[$name] = array();
		}

		if (!is_array($this->formatCallbacks[$name])) {
			$this->formatCallbacks[$name] = array();
		}

		$this->formatCallbacks[$name][] = $obj;
	}

	protected function format(BaseGrid $grid) {
		$output = array();
		foreach ($grid->getData() as $row) {
			$this->applyCallback($row);
			$output[] = $row;
		}
		return $output;
	}

	private function applyCallback(&$row) {
		foreach ($this->formatCallbacks as $key => $callbacks) {
			if (isset($row->{$key})) {
				foreach ($callbacks as $callback) {
					$row->{$key} = call_user_func_array(array($this, $callback), array($row->{$key}));
				}
			}
		}
	}

}
