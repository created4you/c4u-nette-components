<?php

namespace C4U\Components\XlsGenerator;

interface XlsData {

	public function getData();

}