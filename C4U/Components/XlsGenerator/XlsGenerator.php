<?php

namespace C4U\Components\XlsGenerator;

abstract class XlsGenerator {

	/** @var \PHPExcel */
	protected $workbook;

	public function __construct() {
		$this->initialize();
	}

	public function initialize() {
		$this->workbook = new \PHPExcel();
		$this->workbook->setActiveSheetIndex(0);
	}

	protected abstract function createStructure();

	protected function getFilename() {
		return md5(uniqid(rand(), true));
	}

	public function generate() {
		$this->initialize();
		$args = func_get_args();
		$this->createStructure($args);
		$this->publish();
	}

	protected function publish() {
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $this->getFilename() . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer = \PHPExcel_IOFactory::createWriter($this->workbook, 'Excel2007');
		$writer->save('php://output');
		exit();
	}

}