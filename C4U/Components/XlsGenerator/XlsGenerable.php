<?php

namespace C4U\Components\XlsGenerator;

interface XlsGenerable {

	public function getStructure();

}