<?php

namespace C4U\Components\PdfGenerator;

interface PdfGenerable {

	public function getTemplate();
	public function getTemplatePageHeader();
	public function getTemplatePageFooter();
	public function getCssFilename();
	public function getFilename();

}