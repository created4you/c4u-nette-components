<?php

namespace C4U\Components\PdfGenerator;

class PdfGenerator {

	private $saveDir;
	private $wwwDir;
	/** @var \mPDF $pointer */
	private $pointer;
	private $invoicePdf;

	public function __construct($wwwDir, $saveDir, $invoicePdf = null) {
		$this->saveDir = $saveDir;
		$this->wwwDir = $wwwDir;
		$this->invoicePdf = $invoicePdf;
	}

	private function initialize() {
		$this->pointer = new \mPDF();
	}

	public function generate(PdfGenerable $pdfGenerable, $filename) {
		$this->initialize();
		$this->write($pdfGenerable);
		$this->output($filename);
	}

	public function preview(PdfGenerable $pdfGenerable) {
		$this->initialize();
		$this->write($pdfGenerable);
			$this->pointer->Output($pdfGenerable->getFilename(), 'I');
		exit();
	}

	private function write(PdfGenerable $pdfGenerable) {
		$this->pointer->setAutoTopMargin = 'stretch';

		$template = $pdfGenerable->getTemplate();
		$css = $pdfGenerable->getCssFileName();

		if ($css) {
			$css = file_get_contents($css);
			$this->pointer->WriteHTML($css, 1);
		}

		$this->pointer->WriteHTML((string)$template);
	}

	public function getFilename() {
		return md5(time()) . '.pdf';
	}

	public function getOutputPath() {
		return $this->wwwDir . '/' . $this->saveDir . '/';
	}

	private function output($filename) {
		$file = $this->getOutputPath() . $filename;
		$this->pointer->Output($file, 'F');
	}

	public function browserOutput() {
		$this->pointer->Output($this->getFilename());
	}

}