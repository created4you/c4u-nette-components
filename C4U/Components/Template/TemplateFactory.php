<?php

namespace C4U\Components\Template;

use C4U\Date\DateUtils;
use Nette\Latte\Engine;
use Nette\Templating\FileTemplate;

class TemplateFactory {

	public function createTemplate($file) {
		$latte = new \Latte\Engine;
		$template = new \Nette\Bridges\ApplicationLatte\Template($latte);
		$template->setFile($file);
		$template->addFilter('currency', 'C4U\Components\Helpers\Helpers::currency');
		$template->addFilter('currencyFloat', 'C4U\Components\Helpers\Helpers::currencyFloat');
		$template->dateUtils = new DateUtils();
		return $template;
	}
}
