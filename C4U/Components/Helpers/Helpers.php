<?php

namespace C4U\Components\Helpers;

class Helpers {

	public static $currencyCodes = array(
		'CZK' => 'Kč',
		'EUR' => 'EUR',
	);

	public static function currency($value) {
		return str_replace(" ", "\xc2\xa0", number_format($value, 0, "", " ")) . "\xc2\xa0Kč";
	}

	public static function currencyFloat($value, $precision = 2, $currencyCode = 'CZK') {
		$currencyCode = isset(self::$currencyCodes[$currencyCode]) ? self::$currencyCodes[$currencyCode] : $currencyCode;
		return str_replace(" ", "\xc2\xa0", number_format($value, $precision, ",", " ")) . "\xc2\xa0" . $currencyCode;
	}

	public static function currencyFloatNbsp($value, $precision = 2, $currencyCode = 'CZK') {
		$currencyCode = isset(self::$currencyCodes[$currencyCode]) ? self::$currencyCodes[$currencyCode] : $currencyCode;
		return str_replace(" ", "\xc2\xa0", number_format($value, $precision, ",", "&nbsp;")) . "\xc2\xa0" . $currencyCode;
	}

}
