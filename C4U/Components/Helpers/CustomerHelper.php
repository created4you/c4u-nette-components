<?php

namespace C4U\Components\Helpers;

class CustomerHelper {

	public static function formatName($degreeBefore, $degreeBefore2, $name, $surname, $degreeAfter, $separator = ' ') {
		$output = array();

		if ($degreeBefore) $output[] = $degreeBefore;
		if ($degreeBefore2) $output[] = $degreeBefore2;
		if ($name) $output[] = $name;
		if ($surname) $output[] = $surname;
		if ($degreeAfter) $output[] = $degreeAfter;

		return implode($separator, $output);
	}

	public static function formatAddress($separator = ', ') {
		$args = func_get_args();
		if (count($args) < 2) return '';
		unset($args[0]);

		$output = array();
		foreach ($args as $arg) {
			if ($arg) $output[] = $arg;
		}
		return implode($separator, $output);
	}

}
