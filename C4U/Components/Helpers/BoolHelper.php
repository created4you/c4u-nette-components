<?php

namespace C4U\Components\Helpers;

class BoolHelper {

	public static function formatBool($value) {
		return $value ? 'Ano' : 'Ne';
	}

	public static function formatBool3State($value) {
		if ($value === null) {
			return '-';
		} else if ($value === 0 || $value === false) {
			return 'Ne';
		} else {
			return 'Ano';
		}
	}

}
