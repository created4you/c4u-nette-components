<?php

namespace C4U\Components\Menu\Renderers;

interface IMenuRenderer {

	public function getNodeLiClass();
	public function getNodeAClass();
	public function getBadgeClass();

}