<?php

namespace C4U\Components\Menu\Renderers;

class MenuBootstrap4Renderer implements IMenuRenderer {

	public function getNodeLiClass() {
		return 'nav-item';
	}

	public function getNodeAClass() {
		return 'nav-link';
	}

	public function getBadgeClass() {
		return 'badge align-self-center ml-auto';
	}

}
