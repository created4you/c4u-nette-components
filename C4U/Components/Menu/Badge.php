<?php

namespace C4U\Components\Menu;

class Badge {

	public $value;
	public $color;
	public $showIfEmpty = false;

	/**
	 * Badge constructor.
	 * @param $value
	 * @param $color
	 * @param bool $showIfEmpty
	 */
	public function __construct($value, $color, bool $showIfEmpty) {
		$this->value = $value;
		$this->color = $color;
		$this->showIfEmpty = $showIfEmpty;
	}

}
