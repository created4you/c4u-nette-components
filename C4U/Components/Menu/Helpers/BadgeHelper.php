<?php

namespace C4U\Components\Menu\Helpers;

use Nette\Utils\Html;

class BadgeHelper {

	public static $class = 'label label-primary';
	public static $handleAsNumber = true;

	public static function create($title = null) {
		if (!$title) return '';
		$badge = Html::el('span');
		$badge->class = self::$class;

		if (self::$handleAsNumber) {
			if (is_numeric($title) && (int)$title >= 100) {
				$title = "99+";
			}
		}

		$badge->setText($title);

		return $badge;
	}

}
