<?php

namespace C4U\Components\Menu;

use Nette\InvalidStateException;

class Node extends Menu {

	protected $title;
	protected $signal;
	protected $paramametrs;
	protected $current = NULL;
	protected $allowed = NULL;
	protected $resource;
	protected $privilege;
	protected $icon;
	protected $isTitle = false;
	protected $badge;

	/**
	 * @var Node
	 */
	protected $parentNode = NULL;

	public function setIsTitle($isTitle) {
		$this->isTitle = $isTitle;
	}

	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	public function setSignal($signal) {
		$this->signal = $signal;
		return $this;
	}

	public function setParametrs(array $parameters) {
		$this->paramametrs = $parameters;
		return $this;
	}

	public function setLink($link) {
		$this->link = $link;
		return $this;
	}

	public function getIsTitle() {
		return $this->isTitle;
	}

	public function setCurrent($current) {
		$this->current = (bool)$current;
		if ($this->current) {
			$parent = $this->parentNode;
			while ($parent instanceof Node) {
				$parent->setCurrent(TRUE);
				$parent = $parent->parentNode;
			}
		}
		return $this;
	}

	public function setAllowed($allowed) {
		$this->allowed = $allowed;
		return $this;
	}

	public function setParentNode($parentNode) {
		$this->parentNode = $parentNode;
		return $this;
	}

	public function setResource($resource) {
		$this->resource = $resource;
		return $this;
	}

	public function setPrivilege($privilege) {
		$this->privilege = $privilege;
		return $this;
	}
	
	public function setIcon($icon) {
		$this->icon = $icon;
		return $this;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getSignal() {
		return $this->signal;
	}

	public function getIcon() {
		return $this->icon;
	}

	public function getBadge() {
		return $this->badge;
	}

	public function getLink() {
		if (!$this->getSignal()) {
			return 'javascript:void(0)';
		}
		$presenter = $this->getPresenter(FALSE);
		$link = $presenter->link($this->getSignal(), $this->getParameters());
		return $link;
	}

	public function isCurrent() {
		return $this->current;
	}

	public function isAllowed() {
		if (!$this->privilege || !$this->resource) return true;
		if ($this->privilege && $this->resource && !$this->user) throw new InvalidStateException('No reference to Nette\Security\User');
		if ($this->allowed === null) {
			if (isset($this->resource, $this->privilege)) {
				$this->allowed = $this->user->isAllowed($this->resource, $this->privilege);
			} else {
				$this->allowed = true;
			}
		}
		return $this->allowed;
	}

	public function addBadge($value, $color, $showWhenEmpty = false) {
		$this->badge = new Badge($value, $color, $showWhenEmpty);
		return $this;
	}
}
