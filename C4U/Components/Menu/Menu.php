<?php

namespace C4U\Components\Menu;

use C4U\Components\BaseComponent;
use C4U\Components\Menu\Helpers\BadgeHelper;
use C4U\Components\Menu\Renderers\IMenuRenderer;
use C4U\Components\Menu\Renderers\MenuDefaultRenderer;
use C4U\Translator\Translator;
use Nette\Security\IAuthorizator;
use Nette\Security\User;
use Nette\Application\IRouter;
use Nette\Http\IRequest;

class Menu extends BaseComponent {
	protected $childs = array();

	/** @var  User */
	protected $user;

	protected $id;

	protected $count = 0;

	private $class;
	private $title;
	private $ulClass = 'nav nav-list';

	private $router;
	private $request;
	protected $menuRenderer;

	public function __construct($id = null, IRouter $router = null, IRequest $request = null, IMenuRenderer $menuRenderer = null) {
		$this->id = $id;
		$this->router = $router;
		$this->request = $request;
		$this->menuRenderer = $menuRenderer ? $menuRenderer : new MenuDefaultRenderer();
	}

	/**
	 * @param string $title
	 * @param string $signal
	 * @param array $parameters
	 * @return Node
	 */

	public function addTitle($name) {
		$node = new Node($this->id . "_" . $this->count++);
		$node->setTitle($name);
		$node->setParentNode($this);
		$node->setIsTitle(true);
		$this->childs[] = $node;
		return $node;
	}

	public function addNode($title, $signal = NULL, array $parameters = array()) {
		$node = new Node;
		$node->setParentNode($this);
		$node->setTitle($title);
		$node->setSignal($signal);
		$node->isCurrent();
		if (array_key_exists('icon', $parameters)) {
			$node->setIcon($parameters['icon']);
			$parameters['icon'] = NULL;
		}
		if (array_key_exists('param', $parameters)) {
			$node->setIcon($parameters['param']);
		}
		$node->setParametrs($parameters);
		$node->user = $this->user;
		$this->childs[] = $node;

		return $node;
	}

	public function addNodeWithBadge($title, $signal = NULL, array $parameters = array(), $badgeContent = null) {
		$node = $this->addNode($title, $signal, $parameters);
		if ($badgeContent) $node->setTitle($node->getTitle() . BadgeHelper::create($badgeContent));

		return $node;
	}

	public function removeNode($title) {
		foreach ($this->childs as $key => $child) {
			if ($child->getTitle() == $title) {
				unset($this->childs[$key]);
			}
		}
	}

	public function isEmpty() {
		return !((bool)$this->count);
	}

	public function getChilds() {
		return $this->childs;
	}

	public function getId() {
		return $this->id;
	}

	public function hasAnyAllowedChild() {
		foreach ($this->childs as $child) {
			if ($child->isAllowed()) {
				return true;
			}
		}
		return false;
	}

	public function setClass($class) {
		$this->class = $class;
	}

	public function setUlClass($ulClass) {
		$this->ulClass = $ulClass;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function render() {
		$template = $this->createTemplate();
		$template->setFile(dirname(__FILE__) . "/menu.phtml");
		$template->list = $this->childs;
		$template->id = $this->id;
		$template->class = $this->class;
		$template->ulClass = $this->ulClass;
		$template->title = $this->title;
		$template->renderer = $this->menuRenderer;
		$template->render();
	}

	public function setUser(User $user, IAuthorizator $authorizator) {
		$this->user = $user;
		$this->user->setAuthorizator($authorizator);
	}

	public function getPresenterName() {
		if (!($this->router && $this->request)) return null;
		$params = $this->router->match($this->request);
		return isset($params['presenter']) ? $params['presenter'] : null;
	}

	public function getActionName() {
		if (!($this->router && $this->request)) return null;
		$params = $this->router->match($this->request);
		return isset($params['action']) ? $params['action'] : null;
	}

	public function getPresenterParam($name) {
		if (!($this->router && $this->request)) return null;
		$params = $this->router->match($this->request);
		return isset($params[$name]) ? $params[$name] : null;
	}

	public function translate(Translator $translator) {
		/** @var Node $node */
		foreach ($this->getChilds() as $node) {
			$node->setTitle($translator->translate($node->getTitle()));
		}
	}

	public function setBadgeClass($class) {
		BadgeHelper::$class = $class;
	}

	public function setBadgeHandleAsNumber($value = true) {
		BadgeHelper::$handleAsNumber = $value;
	}

}

