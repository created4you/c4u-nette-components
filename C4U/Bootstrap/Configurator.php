<?php

namespace C4U\Bootstrap;

use Nette\Http\RequestFactory;

class Configurator extends \Nette\Configurator {

	protected function getDefaultParameters()
	{
		$parameters = parent::getDefaultParameters();

		$rf = new RequestFactory();
		$baseUrl = rtrim($rf->createHttpRequest()->getUrl()->getBaseUrl(), '/');
		$parameters['baseUrl'] = $baseUrl;
		$parameters['basePath'] = preg_replace('#https?://[^/]+#A', '', $baseUrl);

		return $parameters;
	}
}
