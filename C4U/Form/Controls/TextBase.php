<?php

namespace C4U\Form\Controls;

use Nette\Utils\Strings;

class TextBase extends \Nette\Forms\Controls\TextBase {

	public function getValue() {
		return $this->value === Strings::trim($this->translate($this->emptyValue)) ? NULL : $this->value;
	}

}