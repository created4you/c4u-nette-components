<?php

namespace C4U\Form;

use C4U\Model\Entity;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\BaseControl;

abstract class BaseForm extends Container {

	public function __construct() {
		$this->assembleForm();
	}

	protected function assembleForm() {
		$this->defineForm();
	}

	abstract protected function defineForm();

	abstract protected function setDefaultValues();

	public function submitted(BaseForm $form) {

	}

	abstract public function save();

	public function getSuccessfulMessage() {
		return 'Data byla v pořádku uložena.';
	}

	public function getErrorMessage() {
		return 'Při ukládání dat se vyskytla chyba. Zkuste to prosím znovu.';
	}

	protected function mapToEntity(Entity $entity, $values, $prefix = null) {
		foreach ($values as $key => $value) {
			if ($prefix) {
				if (strpos($key, $prefix) !== 0) {
					continue;
				}
				$key = substr($key, strlen($prefix), strlen($key));
			}
			if (property_exists($entity, $key)) {
				$entity->{$key} = $value;
			}
		}
		return $entity;
	}

	protected function entity2formValues(?Entity $entity, $prefix = null) {
		if ($entity) {
			$values = get_object_vars($entity);
			$this->entityArray2formValues($values, $prefix);
		}
	}

	protected function entityArray2formValues(array $entityArray, $prefix = null) {
		foreach ($entityArray as $key => $value) {
			if ($prefix) {
				$key = $prefix . $key;
			}
			if (isset($this[$key])) {
				try {
					$this[$key]->setDefaultValue($value);
				} catch (\Exception $e) {
					;
				}
			}
		}
	}

	protected function mapSelect($fieldList, $keyName, array $titles, $separator = ' - ') {
		$output = array();
		if (is_array($fieldList)) {
			foreach ($fieldList as $field) {
				$value = '';
				$i = 1;
				foreach ($titles as $t) {
					$partValue = (is_array($t) && count($t) > 1) ? $field->{$t[0]} : $field->{$t};
					if (is_array($t) && count($t) > 1) $partValue = mb_substr($partValue, 0, $t[1], 'utf-8');

					if ($i > 1) {
						if ($partValue && $value) $value .= $separator;
					}
					$value .= $partValue;

					$i++;
				}
				$output[$field->{$keyName}] = $value;
			}
		}
		return $output;
	}

}
