<?php

namespace C4U\Form;

use Nette\Security\User;

abstract class EditForm extends BaseForm {

	public $id = 5;
	protected $idRecord = null;

	public function __construct() {
		parent::__construct();
	}

	protected function addCommonFeatures() {
		parent::addCommonFeatures();
	}

	protected function assembleForm() {
		parent::assembleForm();
		// Set default values only if form is in editing mode.
		if (!$this->idRecord) {
			$this->setDefaultValues();
		}
	}

	public function isEdit(?int $idRecord): void {
		if ($idRecord) {
			$this->idRecord = $idRecord;
			$this->setDefaultValues();
		}
	}

	public function getIsEdit(): bool {
		return !!$this->idRecord;
	}

	// Default save function. Can be overriden.
	public function save() {
		if (!$this->idRecord) return $this->saveNew();
		if ($this->idRecord) return $this->update();
		return null;
	}

	abstract protected function saveNew(): ?int;

	abstract protected function update();

}
