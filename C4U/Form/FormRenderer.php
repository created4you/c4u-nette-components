<?php

namespace C4U\Form;

use C4U\Components\Template\TemplateFactory;
use Nette\Forms\Rendering\DefaultFormRenderer;

class FormRenderer extends DefaultFormRenderer {

	private $templateFactory;

	public function __construct(TemplateFactory $templateFactory) {
		$this->templateFactory = $templateFactory;
	}

	public array $wrappers = array(
		'form' => array(
			'container' => 'div',
			'errors' => TRUE,
		),

		'error' => array(
			'container' => 'div class="alert alert-error"',
			'item' => 'div',
		),

		'group' => array(
			'container' => 'div',
			'label' => 'label class=group-name',
			'description' => 'p',
		),

		'controls' => array(
			'container' => 'div class=container',
		),

		'pair' => array(
			'container' => 'div',
			'.required' => 'required',
			'.optional' => NULL,
			'.odd' => NULL,
		),

		'control' => array(
			'container' => 'span class=control',
			'.odd' => NULL,

			'errors' => FALSE,
			'description' => 'small',
			'requiredsuffix' => '',

			'.required' => 'required',
			'.text' => 'text',
			'.password' => 'text',
			'.file' => 'text',
			'.submit' => 'button btn',
			'.image' => 'imagebutton',
			'.button' => 'btn',
		),

		'label' => array(
			'container' => 'th',
			'suffix' => NULL,
			'requiredsuffix' => ' <span class="label label-important">Povinné</span>',
		),

		'hidden' => array(
			'container' => 'div',
		),
	);

	public function renderBegin(): string {
		$beginTemplate = $this->templateFactory->createTemplate(__DIR__ . '/CustomForm/formBegin.latte');
		if ($this->form->isSubmitted() && $this->form->isValid() && !$this->form->hasErrors()) {
			$beginTemplate->success = $this->form->getSuccessfulMessage();
		}
		$output = $beginTemplate;
		$output .= parent::renderBegin();
		return $output;
	}

	public function renderEnd(): string {
		$endTemplate = $this->templateFactory->createTemplate(__DIR__ . '/CustomForm/formEnd.latte');
		$output = parent::renderEnd();
		$output .= $endTemplate;
		return $output;
	}

}
