<?php

namespace C4U\Form;

use C4U\Form\Controls\TextNullableInput;
use Nette\Forms\Controls\SelectBox;
use Nette\Application\UI\Form;

class Container extends Form {

	public function addTextNullable($name, $label = NULL, $cols = NULL, $maxLength = NULL) {
		$control = new TextNullableInput($label, $maxLength);
		$control->setHtmlAttribute('size', $cols);
		return $this[$name] = $control;
	}

	public function addSelectBool($name, $label = NULL, $withPrompt = true) {
		$items = array(
			0 => 'Ne',
			1 => 'Ano',
		);
		$control = new SelectBox($label, $items);
		if ($withPrompt) {
			$control->setPrompt('- Vyberte -');
		}
		return $this[$name] = $control;
	}

}
